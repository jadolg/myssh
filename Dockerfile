FROM ubuntu:16.04
MAINTAINER Jorge Alberto Diaz Orozco <diazorozcoj@gmail.com>
LABEL description='just ssh on 443 and docker to use as remote docker instance'

RUN apt-get update && apt-get install -y openssh-server docker.io
RUN mkdir /var/run/sshd
RUN echo 'root:qweqwe123!' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN echo 'KexAlgorithms diffie-hellman-group1-sha1,diffie-hellman-group-exchange-sha1' >> /etc/ssh/sshd_config
RUN echo 'Port 443' >> /etc/ssh/sshd_config
RUN mkdir -p /home/docs


# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

EXPOSE 22 443
CMD ["/usr/sbin/sshd", "-D"]
